import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.scss']
})
export class CreateAppointmentComponent implements OnInit {
  appointmentForm: FormGroup;
  dateCount = 0;

  constructor(private fb: FormBuilder,
              private router: Router,
              private db: AngularFirestore) { }

  ngOnInit() {
    this.appointmentForm = this.fb.group({
      id: this.db.createId(),
      title: ['', Validators.required],
      date: ['', Validators.required],
      availableTime: this.fb.array([])
    });
    for (let i = 0; i < 1; i++) {
      this.addDateOption();
    }
  }

  initDateOption(): FormGroup {
    return this.fb.group({
      time: ['', [Validators.required]],
      slots: [1, [Validators.required]],
      available: [null]
    });
  }

  addDateOption() {
    const times = this.appointmentForm.controls.availableTime as FormArray;
    const time = this.initDateOption();
    times.push(time);
    this.dateCount += 1;
  }

  removeDateOption(i: number) {
    const times = this.appointmentForm.controls.availableTime as FormArray;
    times.removeAt(i);
    this.dateCount -= 1;
  }

  save() {
    const availableDateData = this.getDataForAvailableDate();
    this.db.collection('availableDates').doc(this.appointmentForm.value.id).set(availableDateData)
      .then(() => {
        this.setDataForAvailableTimes();
        this.router.navigate(['/home']).then();
      });
  }

  private getDataForAvailableDate() {
    const formValue = this.appointmentForm.value;
    return {
      id: formValue.id,
      title: formValue.title,
      date: formValue.date,
    };
  }

  private setDataForAvailableTimes() {
    const formValue = this.appointmentForm.value;
    const times = this.appointmentForm.controls.availableTime as FormArray;
    times.controls.forEach(time => {
      time.value.slots = parseInt(time.value.slots, 10);
      time.value.available = time.value.slots;
      const availableTime = {
        ...time.value,
        id: this.db.createId(),
        available_date_id: formValue.id
      };
      this.db.collection('availableTimes').doc(availableTime.id).set(availableTime).then();
    });
  }

}
