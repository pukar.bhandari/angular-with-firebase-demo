export interface Appointment {
  availableTime: Time[];
  date: string;
}

export interface Time {
  available: number;
  slots: number;
  time: string;
}
