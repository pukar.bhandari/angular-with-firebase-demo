import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AngularFirestore} from '@angular/fire/firestore';
import * as moment from 'moment';
import {mergeMap, takeWhile} from 'rxjs/operators';
import * as firebase from 'firebase/app';

interface Date {
  id: string;
  date: string;
}

interface Time {
  id: string;
  time: string;
  available_date_id: string;
  slots: number;
  available: number;
}

@Component({
  selector: 'app-add-appointment',
  templateUrl: './add-appointment.component.html',
  styleUrls: ['./add-appointment.component.scss']
})
export class AddAppointmentComponent implements OnInit, OnDestroy {
  scheduleForm: FormGroup;
  availableDates: Date[] = [];
  availableTimes = {};
  allTime: Time[] = [];

  private subscriptionState = true;
  constructor(private fb: FormBuilder,
              private router: Router,
              private db: AngularFirestore) { }

  ngOnInit() {
    this.scheduleForm = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      email: ['', Validators.required],
      date: ['', Validators.required],
      time: ['', Validators.required]
    });
    this.getAvailableDates();
  }

  ngOnDestroy() {
    this.subscriptionState = false;
  }

  save() {
    const selectedTimeId = this.getSelectedDateTime();
    const docRef = this.db.collection('availableTimes').doc(selectedTimeId);
    this.scheduleForm.value.date = firebase.firestore.Timestamp.fromDate(moment(this.scheduleForm.value.date).toDate());
    this.scheduleForm.value.id = this.db.createId();
    this.db.collection('appointments').doc(this.scheduleForm.value.id).set(this.scheduleForm.value).then(() => {
      docRef.update({
        available: firebase.firestore.FieldValue.increment(-1)
      }).then (() => {
        this.router.navigate(['/home']).then();
      });
    });
  }

  getAvailableDates() {
    const availableDatesRef = this.db.collection('availableDates');
    availableDatesRef.valueChanges()
      .pipe(
        takeWhile(() => this.subscriptionState),
        mergeMap((availableDate: Date[]) => this.getAvailableTime(availableDate))
      )
      .subscribe(
        (availableTime: Time[]) => this.setAvailableTime(availableTime),
        err => console.log(err)
      );
  }

  getAvailableTime(availableDate: Date[]) {
    this.availableDates = availableDate;
    const availableTimeRef = this.db.collection('availableTimes');
    return availableTimeRef.valueChanges();
  }

  setAvailableTime(availableTime: Time[]) {
    this.allTime = availableTime;
    this.availableDates.forEach(date => {
      this.availableTimes[date.date] = availableTime.filter(time => (time.available_date_id === date.id && time.available > 0));
    });
  }

  getSelectedDateTime(): string {
    const selectedDate = this.availableDates.filter(available => available.date ===  this.scheduleForm.value.date);
    let selectedDateId;
    let selectedTimeId;
    if (selectedDate.length) {
      selectedDateId = selectedDate[0].id;
    }
    const selectedTime = this.allTime.filter(available => (
        available.available_date_id === selectedDateId && available.time === this.scheduleForm.value.time
      )
    );
    if (selectedTime.length) {
      selectedTimeId = selectedTime[0].id;
    }
    return selectedTimeId;
  }

}
