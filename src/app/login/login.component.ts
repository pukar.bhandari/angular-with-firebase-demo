import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  invalid = false;

  constructor(private fb: FormBuilder, private router: Router, private auth: AngularFireAuth) {
  }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onAuthorize(): void {
    this.auth.signInWithEmailAndPassword(this.loginForm.value.email, this.loginForm.value.password).then((loggedUser) => {
      loggedUser.user.getIdToken().then(token => {
        localStorage.setItem('idToken', token);
        this.onAuthComplete();
      });
    }).catch(err => {
      console.log(err.message);
    });
  }

  onAuthComplete(): void {
    this.router.navigate(['/home']);
  }
}
