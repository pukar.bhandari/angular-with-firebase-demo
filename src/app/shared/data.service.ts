import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class DataService {
  private isAuth = false;
  private dataSource = new BehaviorSubject(this.isAuth);
  data = this.dataSource.asObservable();

  constructor() { }

  updatedDataSelection(data: boolean) {
    this.dataSource.next(data);
  }
}
