import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HomeGuard implements CanActivate {
  constructor(private router: Router ) { }

  canActivate() {
    const loginToken = localStorage.getItem('idToken');
    if (loginToken) {
      return true;
    }
    this.router.navigate(['/login']).then();
    return false;
  }
}
