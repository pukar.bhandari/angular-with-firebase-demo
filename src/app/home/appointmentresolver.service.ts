import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {catchError} from 'rxjs/operators';
import {EMPTY} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppointmentResolverService implements Resolve<any>  {
  constructor(private db: AngularFirestore) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.db.collection('appointments').get().pipe(
      catchError((error) => {
        return EMPTY;
      })
  );
  }
}
