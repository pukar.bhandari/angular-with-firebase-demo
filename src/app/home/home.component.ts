import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {CalendarOptions, DateSelectArg, EventClickArg, EventApi, FullCalendarComponent} from '@fullcalendar/angular';
import * as moment from 'moment';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {takeWhile} from 'rxjs/operators';

interface CalendarEvent {
  id: string;
  title: string;
  start: string;
  end: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('calendar', { static: false }) calendar: FullCalendarComponent;
  appointments = [];
  calendarVisible = true;
  calendarRendered = false;
  currentCalendarData;
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridDay'
    },
    initialView: 'dayGridMonth',
    initialEvents: [], // alternatively, use the `events` setting to fetch from a feed
    weekends: true,
    editable: true,
    selectable: false,
    selectMirror: true,
    dayMaxEvents: true,
    select: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    /* you can update a remote database when these fire:
    eventAdd:
    eventChange:
    eventRemove:
    */
  };
  currentEvents: EventApi[] = [];

  private subscriptionState = true;

  constructor(private db: AngularFirestore, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.activatedRoute.data.pipe(
      takeWhile(() => this.subscriptionState)
    ).subscribe((data: {appointments: any})  => {
      data.appointments.forEach((doc) => {
        const time = doc.data().time.split(':');
        const event: CalendarEvent = {
          id: doc.data().id,
          title: doc.data().name,
          start: moment(doc.data().date.toDate()).set({h: time[0], m: time[1]}).format('YYYY-MM-DD HH:mm'),
          end: moment(doc.data().date.toDate()).set({h: time[0], m: time[1]}).format('YYYY-MM-DD HH:mm'),
        };
        this.calendar.getApi().addEvent(event);
      });
    });
  }

  ngOnDestroy(): void {
    this.subscriptionState = false;
  }

  sendData() {
    // CREATE
    this.db.collection('appointments').doc('appointment1').set({
      name: 'Pukar Bhandari',
      date: '04/19/2021',
      time: '08:00'
    }).then();
  }

  updateData(courseId) {
    const postData = {
      course_name: 'course4_updated'
    };
    const updates = {};
    updates['/courses/' + courseId] = postData;
    return this.db.collection('courses').doc(courseId).update(updates).then();
  }

  removeData(courseId) {
    return this.db.collection('courses').doc(courseId).delete().then();
  }

  handleCalendarToggle() {
    this.calendarVisible = !this.calendarVisible;
  }

  handleWeekendsToggle() {
    const { calendarOptions } = this;
    calendarOptions.weekends = !calendarOptions.weekends;
  }

  handleDateSelect(selectInfo: DateSelectArg) {
    const title = prompt('Please enter a new title for your event');
    const calendarApi = selectInfo.view.calendar;

    calendarApi.unselect(); // clear date selection

    if (title) {
      calendarApi.addEvent({
        id: '1',
        title,
        start: selectInfo.startStr,
        end: selectInfo.endStr,
        allDay: selectInfo.allDay
      });
    }
  }

  handleEventClick(clickInfo: EventClickArg) {
    if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
      clickInfo.event.remove();
    }
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

}

