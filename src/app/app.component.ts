import { Component } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {DataService} from './shared/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isAuth: boolean;
  constructor(private auth: AngularFireAuth, private router: Router, private dataService: DataService) {
    this.checkifAuth();
    this.dataService.data.subscribe(
      isAuth => this.isAuth = isAuth
    );
  }

  logOut(): void {
    this.auth.signOut().then(() => {
      this.dataService.updatedDataSelection(false);
      localStorage.removeItem('idToken');
      this.router.navigate(['/login']).then();
    });
  }

  checkifAuth() {
    this.auth.onAuthStateChanged((user) => {
      if (user) {
        this.dataService.updatedDataSelection(true);
      } else {
        this.dataService.updatedDataSelection(false);
      }
    }).then();
  }
}
