import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  invalid = false;

  constructor(private fb: FormBuilder, private router: Router, private auth: AngularFireAuth, private db: AngularFirestore) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      email: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  onAuthorize(): void {
    this.auth.createUserWithEmailAndPassword(this.registerForm.value.email, this.registerForm.value.password).then(registeredUser => {
      this.db.collection('users').doc(registeredUser.user.uid).set({
        username: this.registerForm.value.username,
        name: this.registerForm.value.name,
        uid: registeredUser.user.uid,
        isAdmin: false
      }).then(() => {
        this.onAuthComplete();
      }).catch(err => {
        this.invalid = true;
        this.registerForm.reset();
      });
    });
  }

  onAuthComplete(): void {
    this.router.navigate(['/home']);
  }
}
