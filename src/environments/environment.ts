// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyDXK1zNfBja_2CYYYFZ7nJCH8G5y__Pz90',
    authDomain: 'fir-demo-d171b.firebaseapp.com',
    databaseURL: 'https://fir-demo-d171b.firebaseio.com',
    projectId: 'fir-demo-d171b',
    storageBucket: 'fir-demo-d171b.appspot.com',
    messagingSenderId: '351133488070',
    appId: '1:351133488070:web:956d7d225b58236efcd8c7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
